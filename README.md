# tu-bi-cam-tren-tiktok-shop
Lưu ý danh sách các từ, cụm từ bị cấm trên TikTok Shop cần tránh
<p style="text-align: justify;">TikTok Shop là kênh mua bán đang làm mưa làm gió trên thị trường hiện nay.Tuy nhiên nếu bạn muốn bán hàng trên nền tảng này thì cần tìm hiểu và lưu lại <a href="https://blogvn.org/cac-tu-bi-cam-tren-tiktok-shop.html"><strong>danh sách từ bị cấm của TikTok Shop</strong></a>. Bởi nếu bạn sử dụng nhiều những từ, cụm từ có tính vi phạm thì hệ thống cấm tài khoản mua bán.</p>
<p style="text-align: justify;">Về cơ bản TikTok Shop cũng không có quá nhiều quy định về câu chữ so với các sàn TMĐT khác. Nếu bạn là người thường xuyên mua hàng thì cũng dễ dàng nhận biết và phân biệt được.</p>
